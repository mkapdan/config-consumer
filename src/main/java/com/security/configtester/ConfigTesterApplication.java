package com.security.configtester;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
public class ConfigTesterApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigTesterApplication.class, args);
    }





    @RestController
    class Greetings{

        @Value("${say.hello:test}")
        String message;

        @GetMapping("/say")
        String sayHello(){
            return message;
        }
    }


}
